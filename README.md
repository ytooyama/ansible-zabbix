# ansible-zabbix

Zabbix ServerとZabbix AgentをAnsibleで構築するもの。Zabbix ServerもZabbix Agentをインストールする監視対象もUbuntu Server 20.04 LTSおよび18.04 LTSを想定しています。

## ファイルについて

### hosts

インベントリファイル。実行対象のノードを記述します。実行対象としてUbuntuを想定しているため、他のディストリビューションを使う場合は書き換えが必要です。

`all:vars`の設定は全てのノードが対象の設定が書かれています。`ansible_python_interpreter`はPython 3を使って実行します。ノードにPython 3がインストールされていないと動きません。`ansible_ssh_private_key_file`は認証に使う秘密鍵です。

### playbook-zbxserver.yaml

Zabbix Serverのインストールを行います。MySQL Serverのrootパスワード、zabbixデータベースのパスワードがハードコードされているので、実際に使うときに修正が必要です。インストールされるZabbix Serverは5.0 LTS。

### playbook-zbxagent.yaml (およびplaybook-zbxagent2.yaml)

Zabbix Agentのインストールを行います。とりあえずAgentを入れるノードもUbuntu Server 20.04/18.04 LTSを想定しますが、上から3つのタスク部分を書き換えれば、他のLinuxでも対応可能です。インストールとZabbix Agentの設定までを行います。

Zabbix Agentの設定のうち、ノード固有の設定については `var/node-ip-address.yml`に記述します。Zabbix Agentをインストールするノードが複数必要な場合はその分のファイルを作成します。`hosts`ファイルに書いたAgentノードと1:1になるようにします。

`playbook-zbxagent2.yaml`はZabbix Agent 2を導入するバージョンです。コンフィグレーションは同様の設定が行われます。

### template/zabbix.conf.php

Zabbix Serverの初期設定のテンプレートです。このPlaybookの初期バージョンはZabbix Serverの初期設定は手動で行う前提にしていたが設定を入力するのが面倒になったため、テンプレートをリモートにコピーする方式に切り替えました。Zabbix Serverのバージョン変更をする場合、テンプレートが変わる可能性があるので注意が必要です。 現時点では `/usr/share/zabbix/conf/zabbix.conf.php.example`のファイルをベースにしていて、DBパスワードのみハードコードしています。

## デプロイ方法について

- Zabbix Serverをデプロイします

```bash
% ansible-playbook playbook-zbxserver.yaml -i hosts
```

- `http://zbxserver-ip-address/zabbix`にブラウザーでアクセスして、ログイン（初期ユーザー: Admin/zabbix）。

- [Discovery rulesを構成](https://www.zabbix.com/documentation/5.0/manual/discovery/network_discovery/rule)して、監視対象のIPレンジでディスカバリー機能が利用できるように設定します。

- 次を実施して、監視対象のノードにZabbix Agentをインストールして、Zabbixの監視対象にします。

```bash
% ansible-playbook playbook-zbxagent.yaml -i hosts
(Zabbix Agentをインストールする場合)

% ansible-playbook playbook-zbxagent2.yaml -i hosts
(Zabbix Agent 2をインストールする場合)
```

- 「Monitoring > Discovery」にノードが表示されます
- [Autoregistration actions](https://www.zabbix.com/documentation/5.0/manual/discovery/auto_registration)を構成して、特定のメタデータを持ち、Zabbix Agentが導入されているノードを自動で検知してZabbix Serverの監視対象に登録するように構成します。
